"use strict";

const btn = document.querySelector(".button");
btn.addEventListener("click", (e) => {
  getFilms();
});

function getFilms() {
  return fetch("https://swapi.dev/api/films/")
    .then((response) => {
      if (response.status == 200) {
        btn.style.display = "none";
        return response.json();
      } else {
        alert("the server is not responding , sorry");
        btn.style.display = "block";
      }
    })
    .then((data) => {
      let objInfo = data.results;
      objInfo.forEach((keys, i) => {
        const { title, episode_id, opening_crawl, characters } = keys;
        const wrapper = document.createElement("div");
        wrapper.style.width = 800 + "px";
        const titleFilms = document.createElement("h2");
        const episodeFlms = document.createElement("h5");
        const crawlFilms = document.createElement("p");
        const actorFilms = document.createElement("span");

        document.body.prepend(wrapper);
        wrapper.append(titleFilms, episodeFlms, actorFilms, crawlFilms);
        titleFilms.append(title);
        episodeFlms.append("Episode ", episode_id);
        crawlFilms.append(opening_crawl);
        setTimeout(() => {
          characters.forEach((characters, i) => {
            fetch(characters)
              .then((response) => {
                if (response.status == 200) {
                  return response.json();
                }
              })
              .then((data) => {
                actorFilms.append(data.name.split(","));
              });
          });
        }, 10000);
      });
    });
}
